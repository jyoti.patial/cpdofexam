package com.agiletestingalliance.cpdofwebapp;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.agiletestingalliance.*;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import java.util.logging.Logger;

public class TestCPDOFCases {

WebDriver driver;
Minimum minm = new Minimum();
Usefulness usefulness = new Usefulness();
AboutCPDOF aboutCPDOF = new AboutCPDOF();
Duration duration = new Duration();
//Count count = new Count();

      public boolean testMinMClass(int value1, int value2) {
        System.out.println("Test if value1>value2 or value1<value2");
        boolean status = false;
	int value = minm.functn(value1, value2);
	if(value==value2){
		status = true;
	}
       return status;
      }

      
      private WebDriver openURL(String browser) {
       System.out.println("Open browser");
       switch(browser){
             case "firefox":
	          System.setProperty("webdriver.gecko.driver","/home/devops/Downloads/geckodriver");
	          driver = new FirefoxDriver();
             break;
             case "chrome":
System.setProperty("webdriver.chrome.driver","/home/devops/Downloads/chromedriver");
                  driver = new ChromeDriver();
             break;
             default:
             break;
       }
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      driver.get("http://localhost/WebApp");
      return driver;      
     }

@Test
public void testMinimum(){
      assertEquals("Verify Value1>Value2", testMinMClass(2,5), true);
}

@Test
public void testCPDOFPage(){

driver = openURL("firefox");
      

       try 
 	{
		Thread.sleep(3000);
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
WebElement certificationText = driver.findElement(By.cssSelector("div>div.col-md-4>p"));
      assertEquals("Verify CpDoF certification", certificationText.getText(), aboutCPDOF.desc()); 

      assertEquals("Verify CPDOF text", "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and cant dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", duration.dur());

      assertEquals("Comparison Text", "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.", usefulness.desc());
	
driver.quit();
      }

@Test
public void testCPDOFPageOnChrome(){
      driver = openURL("chrome");
      driver.quit();
}


}
